package com.cs.childalert;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.VpnService;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.charts.Cartesian;
import com.anychart.core.cartesian.series.Column;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    int clicked = 0;
    private AnyChartView chart;

    Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        button = findViewById( R.id.button );
        chart=findViewById( R.id.any_chart_view );

        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        String connection = sharedPref.getString("current","");

        if(connection!=null && connection.equals( "false")){
            clicked = 0;
            button.setBackgroundResource( R.drawable.button_turnoff );
        }else if(connection!=null&& connection.equals("true")){
            clicked=1;
            button.setBackgroundResource( R.drawable.button_turn_on );
        }


        if(isMyServiceRunning(Checker.class,getBaseContext())){
            clicked=1;
        }else{
            clicked=0;
        }


        Cartesian cartesian = AnyChart.column();
        cartesian.animation(true);
        List<DataEntry> data = new ArrayList<>();
        data.add(new ValueDataEntry("0", 10));
        data.add(new ValueDataEntry("4", 20));
        data.add(new ValueDataEntry("8", 5));
        data.add(new ValueDataEntry("12", 30));
        data.add(new ValueDataEntry("16", 70));
        data.add(new ValueDataEntry("20", 80));
        data.add(new ValueDataEntry("24", 40));

        Column column = cartesian.column(data);
        cartesian.background( false );
        chart.setChart(cartesian);
    }

    public void connect(View view) {

        if(clicked==0) {

            final Context context = getBaseContext();
            Intent i = new Intent(context, Services.class);
            startForegroundService(i);

            Intent i2 = new Intent(context, Checker.class);
            startForegroundService(i2);

            button.setBackgroundResource( R.drawable.button_turn_on );
            clicked=1;
            SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("current", "true");
            editor.commit();
        }else{
            button.setBackgroundResource( R.drawable.button_turnoff );
            clicked=0;
//            Intent intent = new Intent( getBaseContext(),Checker.class );
//            stopService( intent );
//            Intent intent3 = new Intent( getBaseContext(),Services.class );
            SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("current", "false");
            editor.commit();
        }


        Intent intent = VpnService.prepare(getApplicationContext());
        if (intent != null) {
            startActivityForResult(intent, 0);
        } else {
            onActivityResult(0, RESULT_OK, null);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Intent intent = new Intent(this, MyVpnService.class);
            startService(intent);
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass,Context context) {
        ActivityManager manager = (ActivityManager)context. getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("Service already","running");
                return true;
            }
        }
        Log.i("Service not","running");
        return false;
    }
}
