package com.cs.childalert;

import android.content.Intent;
import android.net.VpnService;
import android.os.ParcelFileDescriptor;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.InetSocketAddress;
import java.nio.channels.DatagramChannel;

import static android.app.Service.START_STICKY;

class MyVpnService extends VpnService {

    private Thread mThread;
    private ParcelFileDescriptor mInterface;
    Builder builder = new Builder();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        mThread = new Thread( new Runnable() {
            @Override
            public void run() {
                try {
                    mInterface = builder.setSession( "MyVPNService" )
                            .addAddress( "195.164.201.2", 24 )
                            .addDnsServer( "8.8.8.8" )
                            .addRoute( "0.0.0.0", 0 ).establish();

                    FileInputStream in = new FileInputStream(
                            mInterface.getFileDescriptor() );

                    FileOutputStream out = new FileOutputStream(
                            mInterface.getFileDescriptor() );

                    DatagramChannel tunnel = DatagramChannel.open();

                    tunnel.connect( new InetSocketAddress( "127.0.0.1", 1194 ) );

                    protect( tunnel.socket() );

                    while (true) {
                        Thread.sleep( 100 );
                    }

                } catch (Exception e) {
                    // Catch any exception
                    e.printStackTrace();
                } finally {
                    try {
                        if (mInterface != null) {
                            mInterface.close();
                            mInterface = null;
                        }
                    } catch (Exception e) {

                    }
                }
            }

        }, "MyVpnRunnable" );

        //start the service
        mThread.start();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
    }
}