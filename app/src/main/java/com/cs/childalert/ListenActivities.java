package com.cs.childalert;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

public class ListenActivities extends Thread {
    boolean exit = false;
    ActivityManager am = null;
    Context context = null;

    public ListenActivities(Context con){
        context = con;
        am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
    }

    public void run(){

        Looper.prepare();

        while(!exit){

            List< ActivityManager.RunningTaskInfo > taskInfo = am.getRunningTasks(MAX_PRIORITY);

            String activityName = taskInfo.get(0).topActivity.getClassName();

            if (activityName.equals("com.android.packageinstaller.UninstallerActivity")) {
                exit = true;
            } else if(activityName.equals("com.android.settings.ManageApplications")) {

                exit=true;
            }
        }
        Looper.loop();
    }
}
