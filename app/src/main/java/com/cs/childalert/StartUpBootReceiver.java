package com.cs.childalert;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.VpnService;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class StartUpBootReceiver extends BroadcastReceiver {

    private Context mContext;

    @Override
    public void onReceive(Context context, Intent intent) {
        mContext= context;

        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {

                    Intent i = new Intent(context, Services.class);
                    mContext.startForegroundService(i);

                    Intent i2 = new Intent(context, Checker.class);
                    mContext.startForegroundService(i2);
        }
    }

}
